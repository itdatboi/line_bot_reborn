import os
import pytest

from app.main import create_app
from app.main.config import main_base

def test_development_config():
    """Test the dev configs"""
    app = create_app("dev")
    assert app.config["SECRET_KEY"] == "TEST_KEY"
    assert app.config["DEBUG"] is True
    assert app.config["SQLALCHEMY_DATABASE_URI"] == "sqlite:///{}".format(os.path.join(main_base, "linebot.db"))

def test_test_config():
    """Test the test configs"""
    app = create_app("test")
    assert app.config["SECRET_KEY"] == "TEST_KEY"
    assert app.config["DEBUG"] is True
    assert app.config["SQLALCHEMY_DATABASE_URI"] == "sqlite:///{}".format(os.path.join(main_base, "linebot_test.db"))

def test_prod_config():
    """Test the prod configs"""
    app = create_app("prod")
    assert app.config["DEBUG"] is False




