import os
import yaml

basedir = os.path.abspath(os.path.dirname(__file__))
main_base = os.path.abspath(os.path.dirname(__name__))

class Config:
    SECRET_KEY = os.environ.get("APP_SECRET_KEY", "TEST_KEY")
    DEBUG = False


class DevConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///{}".format(os.path.join(main_base, "linebot.db"))
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class TestConfig(Config):
    DEBUG = True
    TESTING = True
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = "sqlite:///{}".format(os.path.join(main_base, "linebot_test.db"))


class ProdConfig(Config):
    DEBUG = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = \
        f"postgresql+psycopg2://{os.environ.get('POSTGRES_USER')}:{os.environ.get('POSTGRES_PASSWORD')}@linebot_db/{os.environ.get('POSTGRES_DB')}?client_encoding=utf8"


config_by_name = {"dev": DevConfig, "test": TestConfig, "prod": ProdConfig}

key = Config.SECRET_KEY

def get_config(conf_key):
    try:
        with open(os.path.normpath(os.path.join(basedir, "app_config.yml")), "r") as conf_file:
            conf = yaml.safe_load(conf_file)
    except IOError as io_err:
        print(repr(io_err))
        conf = {}

    return conf.get(conf_key)
