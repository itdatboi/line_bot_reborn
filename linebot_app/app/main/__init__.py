import logging
from logging.handlers import RotatingFileHandler
import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_bcrypt import Bcrypt
from .util.converters import DateConverter

from .config import config_by_name

basedir = os.path.abspath(os.path.dirname(__name__))
db = SQLAlchemy()
flask_bcrypt = Bcrypt()

def setup_logging(config_name):
    if not os.path.exists("logs"):
        os.mkdir("logs")

    log_level = getattr(logging, os.environ.get("LOG_LEVEL", "DEBUG").upper())
    formatter = logging.Formatter("[%(asctime)s] %(filename)s::%(lineno)d - %(levelname)s - %(message)s")
    logger = logging.getLogger("line_bot")
    if logger.hasHandlers():
        logger.handlers.clear()

    logger.setLevel(log_level)
    fh = RotatingFileHandler(os.path.normpath(os.path.join(basedir, 'logs/line_bot.log')),
                             encoding="utf8",
                             maxBytes=10000000,
                             backupCount=5)
    fh.setLevel(log_level)

    fh.setFormatter(formatter)
    logger.addHandler(fh)
    logger.info("Logger initialized")

def create_app(config_name):
    setup_logging(config_name)
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])
    app.url_map.converters["date"] = DateConverter
    db.init_app(app)
    flask_bcrypt.init_app(app)
    CORS(app)

    return app
