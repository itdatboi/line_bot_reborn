import logging

from flask import request
from flask_restplus import Resource, Namespace, fields

from ..service.user_service import get_user_by_id, get_all_users, register_user, login_user, logout_user

LOGGER = logging.getLogger("line_bot.user_controller")

api = Namespace("User", description="User related operations")

# User model used when returning users from API
user_model_returned = api.model('user_return', {
    "id": fields.String(description="User's Line ID"),
    'name': fields.String(description="User's Line display name")
})

# User model used when registering users
user_model_register = api.model('user_register', {
    'username': fields.String(required=True, description="User username"),
    'password': fields.String(required=True, description="User password")
})

# User model used when authing users
user_model_auth = api.model('user_auth', {
    'username': fields.String(required=True, description="User username"),
    'password': fields.String(required=True, description="User password")
})


@api.route("/token")
class userbase(Resource):
    @api.doc("logs in user")
    @api.expect(user_model_auth, validate=True)
    def post(self):
        """logs in the user and provides a token"""
        data = request.json
        return login_user(data)

    def delete(self):
        """Logs out the user and blacklists token"""
        data = request.headers.get("Authorization")
        return logout_user(data)



@api.route("/all")
class UserList(Resource):
    @api.doc("List of all line users")
    @api.marshal_list_with(user_model_returned, envelope="users")
    def get(self):
        """Get a list of all the group line users"""
        return get_all_users()


@api.route("/<id>")
@api.param("id", "The user id")
class User(Resource):
    @api.doc("Get a user from their user id")
    @api.marshal_with(user_model_returned)
    @api.response(404, "User not found")
    def get(self, id):
        """Get a user given their user id"""
        user = get_user_by_id(id)
        if not user:
            api.abort(404, "User not found")

        return user

    @api.response(201, "User successfully registered")
    @api.doc("Register a new user")
    @api.expect(user_model_register, validate=True)
    def post(self, id):
        """Registers a username with an existing line user"""
        data = request.json
        data["id"] = id

        return register_user(data=data)