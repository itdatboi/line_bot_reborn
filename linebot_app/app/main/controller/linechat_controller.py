import logging
from linebot.exceptions import InvalidSignatureError, LineBotApiError
from flask import request
from flask_restplus import Resource, Namespace
from ..util.webhook_handlers import wh_handler
from ..util.bot_commands import send_push_wordcloud, send_push_botstats

LOGGER = logging.getLogger("line_bot.linechat_controller")

api = Namespace("linechat", description="LineChat related operations")



@api.route("/callback")
class LineCallback(Resource):
    @api.hide
    @api.response("200", "OK")
    @api.response("400", "Invalid Signature/No signature Provided")
    def post(self):
        """Callback used by LineChat webhook"""
        try:
            # Grab the LINE signature header
            line_sig = request.headers["X-Line-Signature"]
            LOGGER.debug(f"Given signature: {line_sig}")

            # Get the request body as text
            r_body = request.get_data(as_text=True)
            LOGGER.debug(f"Request body: {r_body}")

            try:
                wh_handler.handle(r_body, signature=line_sig)
            except InvalidSignatureError:
                LOGGER.error("Invalid signature provided")
                api.abort(400)
        except KeyError:
            LOGGER.error("No Signature provided")
            api.abort(400)

        return "OK"


@api.route("/send_group_wordcloud")
class SendGroupWordcloud(Resource):
    @api.response("200", "Message sent")
    @api.response("400", "Response body is incorrect")
    def post(self):
        """Send the specified group a push message"""
        response = {
            "status": "",
            "message": ""
        }
        request_body = request.json
        if request_body is None:
            LOGGER.warning("Request body passed in is not JSON")
            response["status"] = "failure"
            response["message"] = "Response body passed in is not JSON"
            return response, 400

        group_id = request_body.get("group_id")
        user_id = request_body.get("user_id")
        if not group_id or not user_id:
            response["status"] = "failure"
            response["message"] = "You must supply a group id and user id"
            return response, 400

        return send_push_wordcloud(group_id, user_id)

    @api.route("/send_group_botstats")
    class SendGroupBotStats(Resource):
        @api.response("200", "Message sent")
        @api.response("400", "Response body is incorrect")
        def post(self):
            """Send the specified group a push message"""
            response = {
                "status": "",
                "message": ""
            }
            request_body = request.json
            if request_body is None:
                LOGGER.warning("Request body passed in is not JSON")
                response["status"] = "failure"
                response["message"] = "Response body passed in is not JSON"
                return response, 400

            group_id = request_body.get("group_id")
            if not group_id:
                response["status"] = "failure"
                response["message"] = "You must supply a group id and user id"
                return response, 400

            return send_push_botstats(group_id)

