from datetime import timezone
import logging
from flask import request
from flask_restplus import Resource, Namespace
from ..service import linestats_service as linestats
from ..util.decorator import token_required

LOGGER = logging.getLogger("line_bot.linestats_controller")
api = Namespace("linestats", description="Gain insight into the group linechat")


@api.route("/all_message_counts")
class AllMessageCount(Resource):
    @api.response("200", "Request was successful")
    @api.response("401", "Unauthorized")
    # @token_required
    def get(self):
        """Get all message counts (sticker, text) of every group member that has talked at least once"""
        return linestats.get_all_user_message_counts()


# /2018-04-13_-0700/to/2018-04-13_-0700
@api.route("/all_message_counts/<date:start_date>/to/<date:end_date>")
class AllMessageCountTimespan(Resource):
    @api.response("200", "Request was successful")
    def get(self, start_date, end_date):
        """Gets all meesage counts for specified timespan"""
        return linestats.get_all_user_message_counts_by_timespan(start_date, end_date)


@api.route("/all_users_information")
class AllUsers(Resource):
    @api.response("200", "Request was successful")
    def get(self):
        return linestats.get_all_users_information()