import logging
from linebot import LineBotApi, WebhookHandler
from linebot.exceptions import LineBotApiError
from linebot.models import MessageEvent, TextMessage, TextSendMessage, StickerMessage

from ..config import get_config

LOGGER = logging.getLogger("line_bot.lineapi_service")
line_bot_api = LineBotApi(get_config("CHANNEL_ACCESS_TOKEN"))


def line_lookup_user(group_id=None, user_id=None):
    """
    Given an ID, find the user's Line information

    :param group_id: The group ID the user belongs to
    :param user_id: The user id to look up
    :return: dict
    """
    LOGGER.info(f"Looking up group: {group_id} with user id: {user_id}")
    if user_id is None or group_id is None:
        LOGGER.warning(f"User ID or Group ID missing: {user_id}, {group_id}")
        return None
    try:
        profile = line_bot_api.get_group_member_profile(group_id, user_id, timeout=10)
        return profile
    except LineBotApiError as line_err:
        LOGGER.error(f"Error getting user info: {repr(line_err)}")
        return None

def send_push_to_group(group_id, messages):
    """
    Given a group ID, send a push message to that group

    :param group_id: The group ID to send the push message to
    :param message: The messages to send
    :return: None
    """
    # Test group ID: C00ac1f7f93aed51eb3c1ae8174e55b1d

    if group_id is None:
        LOGGER.warning("No group id specified. Will not attempt to send a message")
        return None
    LOGGER.info(f"About to send to group <{group_id}> a push message")

    try:
        line_bot_api.push_message(group_id, messages)
        LOGGER.info("Pushing of message succeeded")
        return {
            "status": "success",
            "message": "Message successfully pushed to group"
        }
    except Exception:
        LOGGER.exception(f"Exception occurred trying to send a push message to group <{group_id}>")
        return None
