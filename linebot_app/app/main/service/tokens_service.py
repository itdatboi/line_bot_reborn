import datetime
import logging

from app.main import db
from app.main.models.Tokens import Tokens
from app.main.models.User import User

LOGGER = logging.getLogger("line_bot.tokens_service")


def get_jwt_token(token):
    LOGGER.info(f"Token given to find: {token}")
    return Tokens.query.filter_by(token=token).first()

def login_token(token):
        response = {
            "status": "success",
            "message": "Successfully logged in",
            "Authorization": token
        }
        return response

def logout_token(token):
    try:
        token = Tokens(token)
        db.session.add(token)
        db.session.commit()
        response = {
            "status": "success",
            "message": "Successfully logged out"
        }
        return response
    except Exception as ex:
        response = {
            "status": "failed",
            "message": f"Could not log out: {repr(ex)}"
        }

        return response


def get_logged_in_user(new_request):
    """Gets the information of the logged in user"""
    auth_token = new_request.headers.get("Authorization")
    if auth_token:

        decoded_auth = User.decode_auth_token(auth_token)
        if not isinstance(decoded_auth, str):
            user = User.query.filter_by(id=decoded_auth[1]).first()
            response = {
                "status": "success",
                "data": {
                    "user_id": user.id,
                    "username": user.username,
                    "admin": user.admin
                }
            }

            return response, 200
        response = {
            "status": "fail",
            "message": decoded_auth
        }
        return response, 401
    else:
        response = {
            "status": "fail",
            "message": "Please provide a valid auth token"
        }
        return response, 401