import logging

from app.main import db
from app.main.models.Message import Message

LOGGER = logging.getLogger("line_bot.message_service")


def save_new_message(message_data):
    """
    Saves a new message to the database
    The message_data dictionary should look something like this:
    {
        "message_id": "",
        "user_id": "",
        "timestamp": "",
        "type": "",
        "content": ""
    }

    :param message_data: A dictionary with message data to insert
    :returns: True if inserted successfully, False otherwise
    """
    if not isinstance(message_data, dict):
        LOGGER.error("Message data is not in dictionary form")
        return False

    # Check if message already exists
    try:
        message = Message.query.filter_by(id=message_data["message_id"]).first()
    except:
        LOGGER.exception("Exception occurred trying to find duplicate message")
        db.session.rollback()
        message= None

    if not message:  # New message
        try:
            LOGGER.info(f"Saving {message_data['type']} message to DB")
            new_message = Message(
                id=message_data["message_id"],
                user_id=message_data["user_id"],
                timestamp=message_data["timestamp"],
                type=message_data["type"],
                content=message_data["content"],
            )
            save_changes(new_message)
            LOGGER.info(f"{message_data['type']} message successfully saved")
            return True
        except KeyError as kerr:
            LOGGER.exception(repr(kerr))
            return False
    else:
        LOGGER.info("There's already a message by that ID")
        return False

def get_message_count_by_type(user_id, m_type=None):
    """
    Returns the counts of the message type passed in

    :param user_id: The user id to look up
    :type user_id: string
    :param my_type: The message type to get the count of
    :return: Integer of the count of messages
    """

    try:
        if m_type is None:
            return Message.query.filter_by(user_id=user_id).count()

        return Message.query.filter_by(user_id=user_id, type=m_type).count()
    except:
        LOGGER.exception("Exception occurred trying to find duplicate message")
        db.session.rollback()
        return 0

def get_all_messages_by_user_id(user_id, m_type=None, limit=None):
    """
    Gets all the messages sent by a user from all time

    :param user_id string: The user id to lookup
    :param m_type string: The type of message to return (sticker, text, etc.) Will return "all" if nothing passed in
    :param limit int: Limit the results
    :return: A list of messages sent by the user
    """
    if m_type is not None:
        if limit is not None and limit > 0:
            return Message.query.filter_by(user_id=user_id, type=m_type).order_by(Message.timestamp).limit(limit).all()
        return Message.query.filter_by(user_id=user_id, type=m_type)

    return Message.query.filter_by(user_id=user_id)


def get_count_messages_by_timespan(user_id, start_date, end_date, m_type=None):
    """
    Gets a count of messages by type within the specified time window

    :param user_id: The user ID to get the messages from
    :type user_id: string
    :param m_type: The type of message to get
    :type m_type: string
    :param start_date: The start date
    :type start_date: Datetime
    :param end_date: The end date
    :type end_date: Datetime
    :return: Count of messages
    """

    try:
        if m_type is None:
            LOGGER.info("Getting timespan of all messages")
            return Message.query.filter_by(user_id=user_id).\
                filter(Message.timestamp >= start_date, Message.timestamp <= end_date).count()
        else:
            LOGGER.info(f"Getting timespan of messages by type: {m_type}")
            return Message.query.filter_by(user_id=user_id). \
                filter_by(type=m_type). \
                filter(Message.timestamp >= start_date, Message.timestamp <= end_date).count()
    except Exception:
        LOGGER.exception("An exception occurred trying to get messages by timestamp")
        return 0


def get_all_messages(m_type=None, limit=None, order="asc"):
    LOGGER.info("Getting all messages")
    if m_type is not None:
        LOGGER.info(f"Getting all messages by type <{m_type}>")
        if limit is not None and limit > 0:
            LOGGER.info(f"Limiting amount of message by <{limit}>")
            return Message.query.filter_by(type=m_type).order_by(getattr(Message.timestamp, order)()).limit(limit).all()
        return Message.query.filter_by(type=m_type).all()
    elif limit is not None:
        LOGGER.info(f"Getting all messages with a specified limit: {limit}")
        return Message.query.order_by(getattr(Message.timestamp, order)()).limit(limit).all()
    return Message.query.all()


def get_count_all_messages():
    """
    Gets a count of all messages

    :return: int
    """
    LOGGER.info("Getting a count of all messages in the DB regardless of type")
    return Message.query.count()


def save_changes(data):
    try:
        db.session.add(data)
        db.session.commit()
    except Exception as ex:
        LOGGER.exception(f"Could not save the db changes: {repr(ex)}")
        db.session.rollback()

