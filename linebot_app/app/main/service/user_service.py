from collections import Counter
import logging
from sqlalchemy import desc

from app.main import db
from app.main.models.User import User
from ..service.tokens_service import login_token, logout_token

LOGGER = logging.getLogger("line_bot.user_service")

def save_new_user(user_data):
    """
    Saves a new user to the database

    The user_data param should look something like this:
    {
        "user_id": "",
        "display_name": "",
        "pic_url": ""
    }

    :param user_data: The user data to add to the users table
    :type user_data: dict
    :return: True if addition was successful; False otherwise
    """
    if not isinstance(user_data, dict):
        return False

    # Check if user already exists
    user = User.query.filter_by(id=user_data["user_id"]).first()
    if not user:  # New user
        try:
            new_user = User(
                id=user_data["user_id"],
                name=user_data["display_name"],
                pic_url=user_data["pic_url"],
                word_counts={},
            )
            save_changes(new_user)
            return True
        except KeyError as kerr:
            LOGGER.exception(repr(kerr))
            raise
    else:
        LOGGER.info("There's already a user by that user id")
        return False


def update_user(user_data):
    """
    Updates an existing user in the database

    The user_data param should look something like this:
    {
        "user_id": "",
        "display_name": "",
        "pic_url": ""
    }

    :param user_data: The user data to update in the users table
    :type user_data: dict
    :return: True if update was successful; False otherwise
    """
    if not isinstance(user_data, dict):
        return False

    try:
        db_user = get_user_by_id(user_data["user_id"])
        db_user.name = user_data["display_name"]
        db_user.pic_url = user_data["pic_url"]

        save_changes(db_user)
        return True
    except KeyError as kerr:
        LOGGER.exception(repr(kerr))
        raise


def register_user(data):
    """Registers a user to their existing line user"""
    response = {
        "status": "",
        "message": ""
    }
    try:
        LOGGER.info("Getting existing user from DB")
        db_user = get_user_by_id(data["id"])
        if not db_user:
            response["status"] = "fail",
            response["message"] = "No user by that ID exists"
            return response, 404

        if db_user.username is not None or db_user.password_hash is not None:
            response["status"] = "fail",
            response["message"] = "User has already been registered"
            return response, 409

        LOGGER.info("Registering a username on existing line user")
        db_user.username = data["username"]
        db_user.password = data["password"]
        LOGGER.info(f"Saving registered user <{data['username']}> in DB")
        save_changes(db_user)
        response["status"] = "success",
        response["message"] = "user successfully registered"
        return response, 201
    except KeyError as kerr:
        LOGGER.exception(repr(kerr))
        response["status"] = "fail",
        response["message"] = "One or more fields missing"
        return response, 400


def login_user(data):
    """
    Logs in a user and gives them an auth token

    :param data: The dict containing username/password combo
    :return: string
    """
    try:
        LOGGER.info(f"Getting user from DB by username: {data['username']}")
        user = get_user_by_username(data["username"])

        LOGGER.info("Got user from DB, checking password...")
        if user and user.check_password(data["password"]):
            LOGGER.info(f"Creating auth token for <{data['username']}>")
            auth_token = user.encode_auth_token(user.id)
            if auth_token:
                response = login_token(auth_token)
                return response, 200
        else:
            response = {
                "status": "fail",
                "message": "Username or password does not match"
            }
            return response, 401
    except KeyError:
        LOGGER.exception("Error occurred trying to login user")
        response = {
            "status": "fail",
            "message": "Error occurred trying to log in user"
        }
        return response, 500

def logout_user(data):
    """
    Logs out a user and blacklists the token so it can't be used again

    :param data: JWT token
    :return: string
    """
    response = {
        "status": "",
        "message": ""
    }
    try:
        if data:
            # It will be in the form 'Bearer <Auth token>'
            auth_token = data.split(" ")[1]
        else:
            auth_token = ''

        if auth_token:
            decoded_auth = User.decode_auth_token(auth_token)

            if not isinstance(decoded_auth, str):
                response = logout_token(auth_token)
                return response, 200
            else:
                response["status"] = "fail"
                response["message"] = decoded_auth
                return response, 401
        else:
            response["status"] = "fail"
            response["message"] = "Please provide a valid auth token"
            return response, 403

    except Exception:
        LOGGER.exception("Error occurred trying to log out user")
        response = {
            "status": "fail",
            "message": "Error occurred trying to log out user"
        }
        return response, 500


def compare_user_and_add(line_user):
    """
    Compares the given user to determine if an existing user of the same ID is in the DB already

    :param line_user: The line user details to find in the DB
    :type line_user: dict
    :return: True if user was added or updated; False otherwise
    """
    try:
        db_user = get_user_by_id(line_user["user_id"])
        update = False
        if not db_user:
            LOGGER.info(f"User <{line_user.get('display_name')}> has not been added to DB yet, adding now")
            return save_new_user(line_user)
        else:  # Compare the user to see if any update is needed
            LOGGER.info(f"Checking if the user <{line_user.get('display_name')}> needs to be updated")
            if db_user.name != line_user["display_name"]:
                update = True
            elif db_user.pic_url != line_user["pic_url"]:
                update = True

        if update:
            LOGGER.info("Updating user in DB with line user")
            return update_user(line_user)
        else:
            LOGGER.info(f"No update needed for line user <{line_user.get('display_name')}>")
    except KeyError as key_err:
        LOGGER.error(f"{repr(key_err)}")
        LOGGER.debug(f"Line_user given to compare: {line_user}")
        return False
    except Exception as ex:
        LOGGER.error(f"Could not add or update user in the database: {repr(ex)}")
        return False


def merge_word_counts(user_id, word_counts) -> None:
    """
    Merges the dictionary of word counts from the processed text into the DB

    :param user_id: The user id to merge the dictionary of
    :type user_id: string
    :param word_counts: The word counts
    :type word_counts: Counter
    :return: None
    """
    try:
        db_user = get_user_by_id(user_id)
        if not db_user:
            LOGGER.error(f"The user with ID <{user_id}> has not been added to the database yet")
            return

        LOGGER.info("Adding the new word counts to the user's existing one")
        user_word_counts = Counter(db_user.word_counts)
        new_word_count = dict(word_counts + user_word_counts)
        db_user.word_counts = new_word_count
        LOGGER.info("Saving new word counts in DB")
        save_changes(db_user)
    except Exception as ex:
        LOGGER.exception(repr(ex))
        raise


def get_all_users() -> list:
    return User.query.all()


def get_user_by_id(user_id) -> User:
    return User.query.filter_by(id=user_id).first()


def get_user_by_username(username) -> User:
    return User.query.filter_by(username=username).first()


def get_user_count() -> int:
    return User.query.count()

def get_all_users_stats() -> dict:
    """
    Gets all the users and returns the word counts with them
    :return: dict of users
    """

    return User.query.order_by(User.name).all()


def save_changes(data):
    try:
        LOGGER.info("Saving DB changes")
        db.session.add(data)
        db.session.commit()
    except Exception as ex:
        LOGGER.exception(f"Could not save the db changes: {repr(ex)}")
        db.session.rollback()
