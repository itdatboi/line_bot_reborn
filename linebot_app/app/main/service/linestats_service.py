from collections import Counter
from datetime import datetime, timezone
import logging
from . import user_service, message_service


LOGGER = logging.getLogger("line_bot.linestats_service")


def get_all_users_information() -> (dict, int):
    """
    Gets all users and their information with word counts

    :return: dict with user data
    """
    LOGGER.info("Getting all user information from DB")

    people = {}
    line_users = user_service.get_all_users_stats()

    if line_users:
        for user in line_users:
            person = {
                "name": user.name,
                "pic_url": user.pic_url,
                "word_counts": Counter(user.word_counts).most_common(5)
            }
            people[user.id] = person
        return people, 200
    else:
        return None, 404

def get_all_user_message_counts():
    LOGGER.info("Getting all users from DB")
    line_users = user_service.get_all_users()

    data = {
        "total": 0,
        "data": {
            "labels": [],
            "datasets": [
                {
                    "label": "text",
                    "borderWidth": 1,
                    "data": []
                },
                {
                    "label": "sticker",
                    "borderWidth": 1,
                    "data": []
                }
            ]
        }
    }

    total = 0
    for l_user in line_users:
        # Add the display name to be shown on the x-axis
        LOGGER.info(f"Getting message count data for {l_user.name}")
        data["data"]["labels"].append(l_user.name)
        total += message_service.get_message_count_by_type(l_user.id)
        text_message_count = message_service.get_message_count_by_type(l_user.id, "text")
        sticker_message_count = message_service.get_message_count_by_type(l_user.id, "sticker")

        data["data"]["datasets"][0]["data"].append(text_message_count)
        data["data"]["datasets"][1]["data"].append(sticker_message_count)

    LOGGER.info(f"Total messages: {total:,}")
    data["total"] = f"{total:,}"
    return data, 200


def get_all_user_message_counts_by_timespan(start_date, end_date):
    LOGGER.info("Setting end_date to end of the day")
    end_date = end_date.replace(hour=23, minute=59, second=59)

    LOGGER.info(f"Start date: {start_date}")
    LOGGER.info(f"End date: {end_date}")

    LOGGER.info("Converting start and end date to UTC")
    start_date_utc = start_date.astimezone(timezone.utc)
    end_date_utc = end_date.astimezone(timezone.utc)
    LOGGER.debug(f"UTC Timezone start date: {start_date_utc.strftime('%Y-%m-%d %H:%M:%S')}")
    LOGGER.debug(f"UTC Timezone end date: {end_date_utc.strftime('%Y-%m-%d %H:%M:%S')}")

    LOGGER.info("Getting all users from DB")
    line_users = user_service.get_all_users()

    data = {
        "total": 0,
        "data": {
            "labels": [],
            "datasets": [
                {
                    "label": "text",
                    "borderWidth": 1,
                    "data": []
                },
                {
                    "label": "sticker",
                    "borderWidth": 1,
                    "data": []
                }
            ]
        }
    }

    total = 0
    for l_user in line_users:
        # Add the display name to be shown on the x-axis
        LOGGER.info(f"Getting message count data for {l_user.name}")
        data["data"]["labels"].append(l_user.name)
        total += message_service.get_count_messages_by_timespan(l_user.id,
                                                                start_date_utc,
                                                                end_date_utc)
        text_message_count = message_service.get_count_messages_by_timespan(l_user.id,
                                                                            start_date_utc,
                                                                            end_date_utc,
                                                                            "text")
        sticker_message_count = message_service.get_count_messages_by_timespan(l_user.id,
                                                                               start_date_utc,
                                                                               end_date_utc,
                                                                               "sticker")

        data["data"]["datasets"][0]["data"].append(text_message_count)
        data["data"]["datasets"][1]["data"].append(sticker_message_count)

    LOGGER.info(f"Total messages: {total:,}")
    data["total"] = f"{total:,}"
    return data, 200