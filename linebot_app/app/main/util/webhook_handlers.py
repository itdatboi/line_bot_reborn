from collections import Counter
from datetime import datetime, timezone
import logging
from nltk.tokenize import TweetTokenizer
from nltk.corpus import stopwords
import string
import time

from linebot import WebhookHandler
from ..service.lineapi_service import line_lookup_user
from linebot.models import MessageEvent, TextMessage, StickerMessage
from ..service import user_service, message_service
from ..util import bot_commands

from ..config import get_config

LOGGER = logging.getLogger("line_bot.webhook_handlers")
wh_handler = WebhookHandler(get_config("CHANNEL_SECRET"))

# Translator for removing punctuation
TRANSLATE_TABLE = dict((ord(char), None) for char in string.punctuation)


def user_check(event):
    """
    Uses the event object to make sure the message came from a group and
    adds or updates the source user to the DB

    :param event: The message event that came in
    :return: None
    """
    source_type = event.source.type
    if source_type != "group":
        LOGGER.warning("This text message did not come from a group chat. Ignoring...")
        return
    source_user = event.source.user_id
    source_group = event.source.group_id
    LOGGER.info(f"Got message from user <{source_user}>, group <{source_group}>")
    LOGGER.info(f"Looking up user info from LINEApi for <{source_user}>")
    if source_group is not None and source_user is not None:
        user_info = line_lookup_user(source_group, source_user)
        LOGGER.debug(f"Line User info: {user_info}")
        LOGGER.info(f"Adding or updating the DB for user <{user_info.display_name}>")

        line_user = {
            "display_name": user_info.display_name,
            "pic_url": user_info.picture_url,
            "user_id": user_info.user_id
        }

        user_service.compare_user_and_add(line_user)
        return True
    else:
        LOGGER.warn("Got message from a user without a user ID or group ID!")
        return False


@wh_handler.add(MessageEvent, message=StickerMessage)
def sticker_handler(event):
    """Handles incoming sticker events"""
    LOGGER.debug(f"Event: {event}")
    check_result = user_check(event)
    if check_result is False:
        return

    source_user = event.source.user_id

    timestamp = datetime.fromtimestamp(int(event.timestamp / 1000.0), tz=timezone.utc)
    LOGGER.info(f"Got message at timestamp {timestamp} (UTC)")
    message_id = event.message.id
    sticker_message = f'{{"id": {event.message.sticker_id}, "package_id": {event.message.package_id}}}'
    LOGGER.debug(f"Sticker message received: {sticker_message}")
    message_data = {
        "message_id": message_id,
        "user_id": source_user,
        "timestamp": timestamp,
        "type": "sticker",
        "content": sticker_message
    }

    result = message_service.save_new_message(message_data)
    if not result:
        LOGGER.warning("Failed to insert raw message into DB")
        return


@wh_handler.add(MessageEvent, message=TextMessage)
def text_handler(event):
    """Handles incoming standard text messages"""
    LOGGER.debug(f"Event: {event}")
    check_result = user_check(event)
    if check_result is False:
        return

    source_user = event.source.user_id

    # Process the text contents with NLTK
    timestamp = datetime.fromtimestamp(int(event.timestamp / 1000.0), tz=timezone.utc)
    LOGGER.info(f"Got message at timestamp {timestamp} (UTC)")
    message_id = event.message.id
    message = event.message.text
    LOGGER.debug(f"Message recieved: {message}")
    if message.startswith("!"):
        LOGGER.info(f"Command given: {message}")
        command = message[1:].lower()
        if command == "wordcloud":
            LOGGER.info(f"Wordcloud for user <{source_user}> requested")
            bot_commands.send_push_wordcloud(event.source.group_id, source_user)
        elif command == "stats":
            bot_commands.send_push_stats(event.source.group_id, source_user)
        elif command == "botstats":
            bot_commands.send_push_botstats(event.source.group_id)
        elif "ayy" in command:
            bot_commands.send_jackie(event.source.group_id)

        # Skip processing the message since it's a command
        return

    # Insert message into DB first before processing in case something goes wrong
    LOGGER.info("Inserting raw message into DB")
    message_data = {
        "message_id": message_id,
        "user_id": source_user,
        "timestamp": timestamp,
        "type": "text",
        "content": message
    }

    result = message_service.save_new_message(message_data)
    if not result:
        LOGGER.warning("Failed to insert raw message into DB")
        return

    try:
        start_time = time.time()
        LOGGER.info("Tokenizing message...")
        tokenizer = TweetTokenizer(preserve_case=False)
        tokens = tokenizer.tokenize(message)
        LOGGER.debug(f"Tokenized message: {tokens}")

        # Remove all punctuation from each word
        LOGGER.info("Removing all punctuation from message...")
        tokens = [w.translate(TRANSLATE_TABLE) for w in tokens]
        LOGGER.debug(f"Words with no punctuation: {tokens}")

        # Remove all stop words from the count
        LOGGER.info("Getting final list of words without punctuations and stop words...")
        stop_words = stopwords.words("english")
        final_words = [w for w in tokens if len(w) > 0 and w not in stop_words]
        # Check if "ayy" in the words spoken
        if any("ayy" in word for word in final_words):
            LOGGER.info("Ayy detected; Sending jackie in...")
            bot_commands.send_jackie(event.source.group_id)
        LOGGER.info("Counting words...")
        word_count = Counter(final_words)
        LOGGER.debug(f"Final word count: {word_count}")
        total_processing_time = time.time() - start_time
        LOGGER.info(f"Message took {total_processing_time:.3f}s to process")

        # Add the counts to the user's DB counts
        LOGGER.info("Merging results with user's existing results")
        user_service.merge_word_counts(source_user, word_count)
        LOGGER.info(f"Finished processing text message for user <{source_user}>")
    except Exception as ex:
        LOGGER.exception("An exception occurred trying to process the text")
