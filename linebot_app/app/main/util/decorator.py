from functools import wraps
from flask import request

from ..service.tokens_service import get_logged_in_user

def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        data, status = get_logged_in_user(request)
        token = data.get("data")

        if not token:
            return data, status

        return f(*args, **kwargs)

    return decorated

def admin_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        data, status = get_logged_in_user(request)
        token_data = data.get("data")

        if not token_data:
            return data, status

        is_admin = data.get("admin")
        if not is_admin:
            response = {
                "status": "fail",
                "message": "Admin privileges required"
            }
            return response, 401

        return f(*args, **kwargs)
    return decorated()