import io
import json

from ..service import message_service, user_service
from ..config import get_config
import logging
import numpy
from PIL import Image
from wordcloud import WordCloud
import cloudinary
import cloudinary.uploader
from cloudinary.api import resources_by_tag, delete_resources_by_tag


LOGGER = logging.getLogger("line_bot.linechat_methods")
cloud_api_key = get_config("CLOUDINARY_API_KEY")
cloud_api_SECRET = get_config("CLOUDINARY_SECRET")
cloud_name = get_config("CLOUDINARY_NAME")
IMAGE_TAG = "linechat_wordcloud"

def cleanup_uploaded_images():
    # Config cloudinary with API key
    cloudinary.config(
        cloud_name=cloud_name,
        api_key=cloud_api_key,
        api_secret=cloud_api_SECRET
    )

    LOGGER.info(f"Deleting all images that are tagged with <{IMAGE_TAG}>")
    delete_resources_by_tag(IMAGE_TAG)
    LOGGER.info("Image cleanup complete")

def create_wordcloud_user(user_id):
    """
    Creates and returns a wordcloud as an image to be sent to the group

    :param user_id: The user ID to create the wordcloud of
    :return:
    """

    # Config cloudinary with API key
    cloudinary.config(
        cloud_name=cloud_name,
        api_key=cloud_api_key,
        api_secret=cloud_api_SECRET
    )

    LOGGER.info(f"Getting user details for <{user_id}>")
    user = user_service.get_user_by_id(user_id)
    word_counts = user.word_counts
    LOGGER.info(f"Got user word counts, creating wordcloud")
    word_cloud = WordCloud(background_color="white", max_words=100)
    word_cloud.generate_from_frequencies(word_counts)
    LOGGER.info("Saving wordcloud in memory")
    wc_image = word_cloud.to_image()
    mem_storage = io.BytesIO()
    wc_image.save(mem_storage, format="png")
    LOGGER.info("Uploading image to cloudinary for hosting")
    response = cloudinary.uploader.upload(mem_storage.getvalue(), tags=IMAGE_TAG)
    LOGGER.info(f"Response from cloudinary: {json.dumps(response)}")
    return response.get("secure_url")