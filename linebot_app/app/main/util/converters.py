from datetime import datetime
from werkzeug.routing import BaseConverter, ValidationError

class DateConverter(BaseConverter):
    """Extracts a date from the given URL Param"""
    def to_python(self, value):
        try:
            # /stats/all_words/2018-04-13_-0700/to/2018-04-13_-0700
            split_time = value.split("_")
            date = split_time[0]
            tz = split_time[1]
            return datetime.strptime(f"{date} 00:00:00 {tz}", '%Y-%m-%d %H:%M:%S %z')
        except ValueError:
            raise ValidationError()

    def to_url(self, value):
        return value.strftime('%Y-%m-%d')