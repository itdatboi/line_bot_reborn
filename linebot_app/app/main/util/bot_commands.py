import logging
from linebot.models import TextSendMessage, ImageSendMessage
from ..service.lineapi_service import send_push_to_group

from ..service import user_service, message_service
from ..util.linechat_methods import create_wordcloud_user

LOGGER = logging.getLogger("line_bot.bot_commands")

def send_push_wordcloud(group_id, user_id):
    """
    image_message = ImageSendMessage(
        original_content_url='https://example.com/original.jpg',
        preview_image_url='https://example.com/preview.jpg'
    )
    :param group_id:
    :param user_id:
    :return:
    """
    LOGGER.info(f"Generating wordcloud for <{user_id}>")
    db_user = user_service.get_user_by_id(user_id)
    wordcloud_link = create_wordcloud_user(user_id)
    image_message = ImageSendMessage(
        original_content_url=wordcloud_link,
        preview_image_url=wordcloud_link
    )
    text_message = TextSendMessage(text=f"Wordcloud for: *{db_user.name}*")
    response = send_push_to_group(group_id, [text_message, image_message])
    # Wait a couple seconds for image to upload to line
    #time.sleep(3)
    #cleanup_uploaded_images()
    return response, 201


def send_push_stats(group_id, user_id):
    """
    Sends a user's stats through a push message

    :param group_id: The group ID to send the message to
    :param user_id: The user ID to run stats for
    :return:
    """

    if not group_id or not user_id:
        LOGGER.warning("No group ID or user ID given")
        return None, 201

    LOGGER.info(f"Generating stats for <{user_id}>")
    db_user = user_service.get_user_by_id(user_id)
    text_messages = message_service.get_message_count_by_type(user_id, m_type="text")
    sticker_messages = message_service.get_message_count_by_type(user_id, m_type="sticker")
    total = text_messages + sticker_messages
    text_to_send = f"""Stats for *{db_user.name}*
Texts sent: {text_messages:,}
Stickers sent: {sticker_messages:,}
Total messages sent: {total:,}
*since 01/08/2019"""

    text_message = TextSendMessage(text=text_to_send)
    response = send_push_to_group(group_id, text_message)
    return response, 201

def send_push_botstats(group_id):
    """
    Sends the bot's stats to the group specified through a push message

    :param group_id: The group ID to send stats to
    :return:
    """

    if not group_id:
        LOGGER.warning("No group ID given for botstats.")
        return None, 201

    LOGGER.info("Generating stats for myself")
    first_message = message_service.get_all_messages(limit=1)[0]
    last_message = message_service.get_all_messages(limit=1, order="desc")[0]
    total_users = user_service.get_user_count()
    total_messages = message_service.get_count_all_messages()
    text_to_send = f"""😂👌Dank Bot Stats👌😂
======================
Users I have stats for: {total_users}
Total group messages: {total_messages:,}
First msg sent: {first_message.timestamp} (UTC)
Last msg sent: {last_message.timestamp} (UTC)
"""
    text_message = TextSendMessage(text=text_to_send)
    response = send_push_to_group(group_id, text_message)
    return response, 201


def send_jackie(group_id):
    """
    Sends the one and only ayyy.jpg

    :param group_id: The group to send the picture to
    :return:
    """
    image_message = ImageSendMessage(
        original_content_url="https://i.imgur.com/tNm8Kxs.jpg",
        preview_image_url="https://i.imgur.com/tNm8Kxs.jpg"
    )
    response = send_push_to_group(group_id, image_message)
    return response, 201