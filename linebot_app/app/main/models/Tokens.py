from .. import db
import datetime

class Tokens(db.Model):
    """
    Model for storing tokens blacklist
    """

    __tablename__ = "tokens"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    token = db.Column(db.String(500), unique=True, nullable=False)
    blacklisted = db.Column(db.Boolean, default=False, nullable=False)
    blacklisted_on = db.Column(db.DateTime, nullable=True)

    def __init__(self, token):
        self.token = token
        self.blacklisted = True
        self.blacklisted_on = datetime.datetime.utcnow()

    def __repr__(self):
        return f"<Token: {self.token}>"

    @staticmethod
    def is_token_blacklisted(auth_token):
        token = Tokens.query.filter_by(token=str(auth_token)).first()
        if token:
            return True
        return False