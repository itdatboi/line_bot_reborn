import datetime
import jwt
import logging

from app.main.models.Tokens import Tokens
from .. import db, flask_bcrypt
from ..config import key
from sqlalchemy_utils.types import JSONType

LOGGER = logging.getLogger("line_bot.User")

class User(db.Model):
    """Modem for storing user details"""
    __tablename__ = "lineuser"

    id = db.Column(db.String(255), primary_key=True)
    name = db.Column(db.String(255))
    pic_url = db.Column(db.String(400))
    status_message = db.Column(db.String(255))
    word_counts = db.Column(JSONType)
    username = db.Column(db.String(50))
    password_hash = db.Column(db.String(100))
    admin = db.Column(db.Boolean, default=False)

    @property
    def password(self):
        raise AttributeError("Password is a write-only field")

    @password.setter
    def password(self, given_password):
        self.password_hash = flask_bcrypt.generate_password_hash(given_password).decode("utf-8")

    def check_password(self, password):
        return flask_bcrypt.check_password_hash(self.password_hash, password)


    @staticmethod
    def encode_auth_token(user_id):
        """
        Generate JWT auth token for user_id
        :param user_id: The user ID to generate a token for
        :return: JWT Token
        """
        try:
            payload = {
                # Expires in 1 day, 5 seconds
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1, seconds=5),
                'iat': datetime.datetime.utcnow(),
                'sub': user_id
            }

            jwt_token = jwt.encode(
                payload,
                key,
                algorithm="HS256"
            ).decode("utf-8")
            LOGGER.info(f"Generated token: {jwt_token}")
            return jwt_token
        except Exception as ex:
            LOGGER.exception("There was an error generating the auth token")

    @staticmethod
    def decode_auth_token(auth_token):
        """
        Decodes the auth token and makes sure it's valid

        :param auth_token: Token to decode
        :return:
        """

        try:
            act_token = auth_token.split(" ")[1]
            payload = jwt.decode(act_token, key)
            is_blacklisted = Tokens.is_token_blacklisted(act_token)
            if is_blacklisted:
                return "The given token has been logged out. Please log in again"
            else:
                return (True, payload.get("sub"))
        except jwt.ExpiredSignatureError:
            LOGGER.warning("Signature of auth token is expired")
            return "Signature of the given token as expired, please log in again"
        except jwt.InvalidTokenError as ex:
            LOGGER.warning(f"Given auth token is invalid <{auth_token}>: {ex}")
            return "The given token is invalid! Please log in again"