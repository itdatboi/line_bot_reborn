from .. import db
from sqlalchemy import ForeignKey
from .User import User

class Message(db.Model):
    """Stores messages that each user sets"""

    __tablename__ = "message"

    id = db.Column(db.String(255), primary_key=True, autoincrement=False)
    user_id = db.Column(db.String(255), ForeignKey(User.id), nullable=False)
    timestamp = db.Column(db.DateTime, nullable=False)
    type = db.Column(db.String(100), nullable=False)
    content = db.Column(db.Text)