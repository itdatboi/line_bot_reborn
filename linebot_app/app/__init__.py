from flask_restplus import Api
from flask import Blueprint

from .main.controller.linechat_controller import api as linechat_ns
from .main.controller.linestats_controller import api as linestats_ns
from .main.controller.user_controller import api as user_ns

blueprint = Blueprint('api', __name__)

api = Api(blueprint,
          title="Dank Linechat Bot",
          version="1.0",
          description="A linechat bot made by Alvin",
          doc="/docs/")

api.add_namespace(linechat_ns, path='/linechat')
api.add_namespace(linestats_ns, path='/linestats')
api.add_namespace(user_ns, path="/user")