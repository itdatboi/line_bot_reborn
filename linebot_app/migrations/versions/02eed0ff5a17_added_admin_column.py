"""Added admin column

Revision ID: 02eed0ff5a17
Revises: fc42421f289c
Create Date: 2019-01-11 10:50:54.732258

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '02eed0ff5a17'
down_revision = 'fc42421f289c'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('lineuser', schema=None) as batch_op:
        batch_op.add_column(sa.Column('admin', sa.Boolean(), nullable=True))

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('lineuser', schema=None) as batch_op:
        batch_op.drop_column('admin')

    # ### end Alembic commands ###
