# Alvin's Linechat Bot
This bot is for providing a callback for linechat group messages and responding and keeping stats.

This bot is just for fun.
### Generating SSL certs using docker-compose
Run in order

1. ```docker-compose run --rm --entrypoint "openssl req -x509 -nodes -newkey rsa:1024 -days 1 -keyout '/etc/letsencrypt/live/alvinlee.stream/privkey.pem' -out '/etc/letsencrypt/live/alvinlee.stream/fullchain.pem' -subj '/CN=localhost'" certbot```
2. ```docker-compose run --rm --entrypoint "rm -Rf /etc/letsencrypt/live/alvinlee.stream && rm -Rf /etc/letsencrypt/archive/alvinlee.stream && rm -Rf /etc/letsencrypt/renewal/alvinlee.stream.conf" certbot```
3. *Remove `--staging` param when ready*
```docker-compose run --rm --entrypoint "certbot certonly --webroot -w /var/www/certbot --staging --email algrn91@gmail.com -d alvinlee.stream -d www.alvinlee.stream --rsa-key-size 4096 --agree-tos --force-renewal --debug-challenges" certbot```